<!DOCTYPE html>
<!--
Vista para la tabla usuarios
-->
<html lang="es">
    <head>
        
    </head>
    <body>
        <main class="container">
            <header id="header">
                
            </header>

            <nav id="mainMenu">
                
            </nav>
        	
            <article id="content">
                <h2>Inicio de session</h2>

                <p>
                    ingrese sus credenciales
                </p>
<?php
              echo form_open(base_url() . "index.php/session/validateSession");
              echo "<table>";
                        
                    echo form_hidden('id', $id);
                    echo "<table>";
                    echo "<tr>";
                    echo "<th>";
                    echo form_label('Usuario', 'user');
                    echo "</th>";
                    echo "<td>";
                    echo form_input('user');
                    echo "</td>";
                    echo "</tr>";
                    
                    echo "<tr>";
                    echo "<th>";
                    echo form_label('contraseña', 'password');
                    echo "</th>";
                    echo "<td>";
                    echo form_input('password');
                    echo "</td>";
                    echo "</tr>";
                    
                    echo "<tr>";
                    echo "<td colspan='2'>";
                    echo form_submit('btnSubmit', 'iniciar session');
                    echo form_reset('btnReset', 'Restablecer');
                    echo "</td>";
                    echo "</tr>";
                    echo "</table>";
                    echo form_close();
                    
                    ?>
                <div id="error">
                    <?php
                     echo validation_errors();
                     ?>
                </div>
                
                <div id="dialog" title="Mensaje del sistema"></div>

            </article>
        </main>
        
        
        
    </body>
</html>
